import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: any;

  constructor(private httpClient: HttpClient) { }

  // GET
  getPosts(){
    this.posts = this.httpClient.get(`https://jsonplaceholder.typicode.com/posts/`);
  }

  ngOnInit() {
    this.getPosts();
  }
}
