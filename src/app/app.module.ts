import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './router/app-routing.module';
// App
import { AppComponent } from './app.component';
// Components
import { PostsComponent } from './components/posts/posts.component';
import { HeaderComponent } from './components/header/header.component';
// Views
import { HomeComponent } from './views/home/home.component';
import { AboutComponent } from './views/about/about.component';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AboutComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
